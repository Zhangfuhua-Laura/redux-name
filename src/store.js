import {applyMiddleware, compose, createStore} from "redux";
import reducers from "./reducers";
import thunk from "redux-thunk";

// 2、 createStore, store中存有reducers
const store = createStore(
  reducers,
  // 为能在浏览器中使用查看redux
  compose(
    applyMiddleware(thunk),
    typeof window === 'object' &&
    typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== "undefined" ?
      window.__REDUX_DEVTOOLS_EXTENSION__() :
      f => f
  )

);

export default store;