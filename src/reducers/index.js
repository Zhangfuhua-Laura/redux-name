import {combineReducers} from "redux";
import nameReducer from "./name";

//3、 将多个的reducer combine 到 reducers里
const reducers = combineReducers({
  nameReducer
});

export default reducers;