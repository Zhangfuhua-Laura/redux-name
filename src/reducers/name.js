import {GET_NAME, SET_NAME} from "../actions/name";

// 4、 创建Reducer，根据action type的类型不同，采取不同的行动去修改state

const initialState = {name: ''};

export default function nameReducer(state=initialState, action) {
  if (action.type === SET_NAME) {
    return {
      ...state,
      name: action.payload
    };
  } else if (action.type === GET_NAME) {
    return {
      ...state,
      name: action.payload
    };
  }  else {
    return state;
  }
}