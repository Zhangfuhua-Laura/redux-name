import React, {Component} from 'react';
import {connect} from "react-redux";
import {setName, getName} from "../actions/name";

// 6、 创建components

class Name extends Component {

  constructor(props, context) {
    super(props, context);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.setName(event.target.value);
  }

  componentDidMount() {
    this.props.getName();
  }

  render() {
    return (
      <div>
        <h1>Hi, {this.props.name}</h1>
        <input type="text" onChange={this.handleChange}/>
      </div>
    );
  }
}

// 8、 实现链接函数的内容
//将state中的值绑定到this.props
const mapStateToProps = ({nameReducer}) => ({
  name: nameReducer.name
});

//将action中定义的方法绑定到this.props
const mapDispatchToProps = (dispatch) => ({
  setName: (name) => (dispatch(setName(name))),
  getName: (name) => (dispatch(getName(name))),
});

// 7、 创建component组件 与 redux 之间的链接
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Name);