import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from "react-redux";
import store from "./store";

ReactDOM.render(
    //1、 裹一层Provide，并且将store传进去
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'));